extends Node2D

var value := 0
onready var box = get_node("Box")
onready var label = get_node("NumberText")
var c0 = Color.from_hsv(0,0,1)
var c1 = Color.from_hsv(0,0.8,1)
var c2 = Color.from_hsv(0.3,0.8,1)
var c3 = Color.from_hsv(0.6,0.8,1)
var c4 = Color.from_hsv(0.9,0.8,1)
var c5 = Color.from_hsv(0.2,0.8,1)
var c6 = Color.from_hsv(0.5,0.8,1)
var c7 = Color.from_hsv(0.8,0.8,1)
var c8 = Color.from_hsv(0.1,0.8,1)
var c9 = Color.from_hsv(0.4,0.8,1)
var boxColors = [c0,c1,c2,c3,c4,c5,c6,c7,c8,c9]

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func ChangeValue(var newValue:int):
	if newValue >= 0 and newValue <= 9:
		value = newValue
		if value == 0:
			label.text = ""
		else:
			label.text = str(value)
		box.modulate = boxColors[value]

func HighlightTile(active:bool):
	if active:
		box.modulate = boxColors[value].darkened(0.3)
	else:
		box.modulate = boxColors[value]