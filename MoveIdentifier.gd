extends Button

var x1 := 0
var x2 := 0
var y1 := 0
var y2 := 0
var add := false

func UpdateButton():
	var additionString = " -"
	if (add): additionString = " +"
	self.text = str(x1, ",", y1, "   ", x2, ",", y2, additionString)
