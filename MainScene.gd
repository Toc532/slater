extends Node

var tile = preload("res://NumberTile.tscn")
onready var prevMoves = get_node("PrevMovesContainer/ScrollContainer/MoveList")
onready var puzzleMoves = get_node("PuzzleMovesContainer/ScrollContainer/MoveList")
onready var particles = get_node("FestiveParticles")
var sizeX := 10
var sizeY := 10
var tileOffsetX = 192
var tileOffsetY = 64
var tileSpacing = 64
var easySteps = 3
var mediumSteps = 5
var hardSteps = 7
var expertSteps = 9
var mouseDownPos
var mouseReleasePos
var selX1 :int = 0
var selX2 :int = 0
var selY1 :int = 0
var selY2 :int = 0
var mouseDown := false
var tileRefs = []
var mat
var puzzleCleared := false

var rng = RandomNumberGenerator.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	rng.randomize()
	puzzleMoves.visible = false
	
	# Add a puzzle
	MakePuzzle(sizeX, sizeY, mediumSteps)
	# Generate Tile Nodes
	for i in range(sizeX):
		var newTiles = []
		for j in range(sizeY):
			var newTile = tile.instance()
			var newPos = Vector2(tileOffsetX + i * tileSpacing, tileOffsetY + j * tileSpacing)
			add_child(newTile)
			newTile.position = newPos
			newTile.ChangeValue(mat[i][j])
			newTiles.append(newTile)
		tileRefs.append(newTiles)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("LMB") or Input.is_action_just_pressed("RMB"):
		mouseDown = true
		GetTileIndicesFromMousePosition(true)
	
	if mouseDown:
		GetTileIndicesFromMousePosition(false)
		# highlight selected tiles
		PerformHighlights()
	
	if Input.is_action_just_released("LMB"):
		MouseRelease(true)
	
	if Input.is_action_just_released("RMB"):
		MouseRelease(false)
	
	if Input.is_action_just_released("redo"):
		var move
		for i in range(prevMoves.previousMoves.size()):
			if prevMoves.previousMoves[i].pressed:
				move = prevMoves.previousMoves[i]
				move.pressed = false
				if prevMoves.PreviousMoveChange(false, move):
					break
	
	if Input.is_action_just_released("simplify"):
		var move
		for i in range(puzzleMoves.previousMoves.size()):
			if puzzleMoves.previousMoves[i].pressed:
				move = puzzleMoves.previousMoves[i]
				move.pressed = false
				if puzzleMoves.PreviousMoveChange(false, move):
					break

func Make2DArray(width, height):
	var array = []
	for i in width:
		array.append([])
		for j in height:
			array[i].append(0)
	return array

func MouseRelease(var lmb:bool):
	mouseDown = false
	
	# Check if no tiles are selected
	if ((selX1 < 0 and selX2 < 0) or \
	   (selX1 >= sizeX and selX2 >= sizeX)) or \
	   ((selY1 < 0 and selY2 < 0) or \
	   (selY1 >= sizeY and selY2 >= sizeY)):
		return false
	
	# clamp indices
	selX1 = clamp(selX1, 0, sizeX-1)
	selX2 = clamp(selX2, 0, sizeX-1)
	selY1 = clamp(selY1, 0, sizeY-1)
	selY2 = clamp(selY2, 0, sizeY-1)
	
	var lowX = min(selX1, selX2)
	var highX = max(selX1, selX2)
	var lowY = min(selY1, selY2)
	var highY = max(selY1, selY2)
	
	return PerformMove(lowX, lowY, highX, highY, !lmb, true)

func GetTileIndicesFromMousePosition(var down:bool):
	mouseDownPos = get_viewport().get_mouse_position();
	if down:
		selX1 = floor((mouseDownPos.x - tileOffsetX)/tileSpacing)
		selY1 = floor((mouseDownPos.y - tileOffsetY)/tileSpacing)
	else:
		selX2 = floor((mouseDownPos.x - tileOffsetX)/tileSpacing)
		selY2 = floor((mouseDownPos.y - tileOffsetY)/tileSpacing)

func PerformHighlights():
	var lowX = min(selX1, selX2)
	var highX = max(selX1, selX2)
	var lowY = min(selY1, selY2)
	var highY = max(selY1, selY2)
	
	for i in range(sizeX):
		for j in range(sizeY):
			if (i >= lowX and i <= highX) and (j >= lowY and j <= highY):
				tileRefs[i][j].HighlightTile(true)
			else:
				tileRefs[i][j].HighlightTile(false)

func PerformMove(var x1:int, var y1:int, var x2:int, var y2:int, var add:bool, var save:bool):
	var tileScripts = []
	var validMove = true
	
	# make array of selected tiles
	for i in range(x1, x2 + 1):
		for j in range(y1, y2 + 1):
			tileScripts.append(tileRefs[i][j])
	
	# Check if move does not change any tiles below 0 or above 9
	for tileScript in tileScripts:
		if (tileScript.value == 9 and add) or (tileScript.value == 0 and !add):
			validMove = false
	
	# Change tiles value +1 or -1 depending on mouse button released
	if validMove:
		var change = -1
		if add: change = 1
		for tiles in tileScripts:
			tiles.ChangeValue(tiles.value + change)
		
		if save: 
			if add: 
				puzzleMoves.AddMove(x1, y1, x2, y2, add)
				puzzleCleared = false
			else: prevMoves.AddMove(x1, y1, x2, y2, add)
		
		if !puzzleCleared:
			if PuzzleClear():
				CelebrateVictory()
				puzzleCleared = true
	else:
		for tiles in tileScripts:
			tiles.HighlightTile(false)
	
	return validMove

func PuzzleClear():
	var tileClear = prevMoves.numberOfActiveMoves <= puzzleMoves.numberOfActiveMoves
	if tileClear:
		for tileRow in tileRefs:
			for tile in tileRow:
				if tile.value > 0:
					tileClear = false
					break
	return tileClear

func MakePuzzle(var width:int, var height:int, var steps:int):
	var x1
	var y1
	var x2
	var y2
	var rw
	var rh
	var xi
	var yi
	var connected
	
	puzzleCleared = false
	
	# Initialize array and set to zeroes
	mat = Make2DArray(width, height)
	
	for k in range(steps):
		# get psuedorandom width and height
		rw = (rng.randi_range(1, width/2) + rng.randi_range(1, width/2)) - 1
		rh = (rng.randi_range(1, height/2) + rng.randi_range(1, height/2)) - 1
		
		if k == 0: # center first rectangle
			x1 = floor(width/2 - rw/2)
			y1 = floor(height/2 - rh/2)
			x2 = x1 + rw-1
			y2 = y1 + rh-1
		else: # make rectangle connected to previous ones
			connected = false
			while !connected:
				x1 = rng.randi_range(0, width - rw)
				y1 = rng.randi_range(0, height - rh)
				x2 = x1 + rw-1
				y2 = y1 + rh-1
				for xi in range(x1, x2+1):
					for yi in range(y1, y2+1):
						if mat[xi][yi] > 0:
							connected = true
		
		# Add the rectangle to the matrix
		for xi in range(x1, x2+1):
			for yi in range(y1, y2+1):
				mat[xi][yi] += 1
		puzzleMoves.AddMove(x1, y1, x2, y2, true)

func CelebrateVictory():
	particles.emitting = true

func _on_CheckBox_toggled(button_pressed):
	puzzleMoves.visible = button_pressed

func _on_EasyButton_button_up():
	prevMoves.ClearMoves()
	puzzleMoves.ClearMoves()
	MakePuzzle(sizeX, sizeY, easySteps)
	for i in range(sizeX):
		for j in range(sizeY):
			tileRefs[i][j].ChangeValue(mat[i][j])

func _on_MediumButton_button_up():
	prevMoves.ClearMoves()
	puzzleMoves.ClearMoves()
	MakePuzzle(sizeX, sizeY, mediumSteps)
	for i in range(sizeX):
		for j in range(sizeY):
			tileRefs[i][j].ChangeValue(mat[i][j])

func _on_HardButton_button_up():
	prevMoves.ClearMoves()
	puzzleMoves.ClearMoves()
	MakePuzzle(sizeX, sizeY, hardSteps)
	for i in range(sizeX):
		for j in range(sizeY):
			tileRefs[i][j].ChangeValue(mat[i][j])


func _on_ExpertButton_button_up():
	prevMoves.ClearMoves()
	puzzleMoves.ClearMoves()
	MakePuzzle(sizeX, sizeY, expertSteps)
	for i in range(sizeX):
		for j in range(sizeY):
			tileRefs[i][j].ChangeValue(mat[i][j])

func _on_ClearMoves_button_up():
	prevMoves.ClearMoves()
	puzzleMoves.ClearMoves()
	for i in range(sizeX):
		for j in range(sizeY):
			mat[i][j] = 0
			tileRefs[i][j].ChangeValue(mat[i][j])
			puzzleCleared = true