extends VBoxContainer

var identifier = preload("res://MoveIdentifier.tscn")
onready var main = get_node("../../../")
onready var numberOfMovesLabel = get_node("../../")
var previousMoves = []
var numberOfActiveMoves = 0
var memorySize = 99

func _ready():
	pass

func AddMove(var x1:int, var y1:int, var x2:int, var y2:int, var add:bool):
	var newIdentifier = identifier.instance()
	add_child(newIdentifier)
	newIdentifier.x1 = x1
	newIdentifier.y1 = y1
	newIdentifier.x2 = x2
	newIdentifier.y2 = y2
	newIdentifier.add = add
	newIdentifier.pressed = true
	newIdentifier.UpdateButton()
	newIdentifier.connect("toggled", self, "PreviousMoveChange", [newIdentifier])
	
	if (previousMoves.size() >= memorySize):
		var oldestInactive 
		# Try to find oldest inactive move
		for i in range(previousMoves.size()-1, -1, -1):
			if !previousMoves[i].pressed:
				oldestInactive = previousMoves[i]
				previousMoves.remove(i)
				break
		if oldestInactive == null:
			oldestInactive = previousMoves.pop_back()
		oldestInactive.queue_free()
	
	# Add new move
	previousMoves.push_front(newIdentifier)
	ChangeNumberOfActiveMoves(1)

func ClearMoves():
	for move in previousMoves:
		move.queue_free()
	previousMoves.clear()
	ChangeNumberOfActiveMoves(-numberOfActiveMoves)

func PreviousMoveChange(var undo:bool, var id):
	if !main.PerformMove(id.x1, id.y1, id.x2, id.y2, undo == id.add, false):
		id.pressed = !id.pressed
		return false
	else:
		if id.pressed:
			ChangeNumberOfActiveMoves(1)
		else:
			ChangeNumberOfActiveMoves(-1)
		return true

func ChangeNumberOfActiveMoves(var change:int):
	numberOfActiveMoves += change
	numberOfMovesLabel.text = str("#Moves = ", numberOfActiveMoves)

func _on_Clear_button_up():
	for i in range(previousMoves.size()-1,-1,-1):
		if !previousMoves[i].pressed:
			previousMoves[i].queue_free()
			previousMoves.remove(i)
